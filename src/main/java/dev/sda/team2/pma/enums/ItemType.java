package dev.sda.team2.pma.enums;

public enum ItemType {
    SERVICE,
    STOCK_ITEM,
    RENTAL
}
